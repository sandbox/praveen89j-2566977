/**
 * @file
 * Custom.js.
 */

jQuery(document).ready(function ($) {
  "use strict";
  jQuery('.form-item-table').hide();
  jQuery('#edit-export-option-0').click(function ($) {
    jQuery('.form-item-table').hide();
  }
  );
  jQuery('#edit-export-option-1').click(function ($) {
    jQuery('.form-item-table').show();
  }
  );
});
