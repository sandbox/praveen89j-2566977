DB Backup module
-----------------
  DB Backup module help to take backup though Exporting site database,
  Provided option to choose whole database dump or specific tables 
  from site database

REQUIREMENTS
------------
  This module requires the following modules:
    You should have to use password, with empty password you 
    can't unable to export database.

INSTALLATION
------------
  As normal as any drupal module.

TROUBLESHOOTING
---------------
 * If you unable to export tables / database:

   - Make sure you have database user privilages.

MAINTAINERS
-----------
  Current maintainers:
    * Praveenkumarj - https://www.drupal.org/u/praveenkumarj
